const { chromium } = require('playwright');
// const { createDocument } = require('./lib/Documenter');
(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext({
      recordVideo: {
          dir:'videos'
      }
  });

  // Open new page
  const page = await context.newPage();

  // Go to https://katalog5.cloud.javan.co.id/
  await page.goto('https://katalog5.cloud.javan.co.id/');

  
  // Click text=Tutup
  await page.click('text=Tutup');
  
  await page.screenshot({path:'E-Katalog.png'})
  // ---------------------
  await context.close();
  await browser.close();
})();